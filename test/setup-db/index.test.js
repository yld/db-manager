/* global db */

const fs = require('fs-extra')
const path = require('path')
const PouchDB = require('pouchdb')

process.env.ALLOW_CONFIG_MUTATIONS = true
process.env.SUPPRESS_NO_CONFIG_WARNING = true

const basePath = path.join(__dirname, '..', '..', 'api', 'db')
const dbPath = path.join(basePath, 'test_db')

const baseConfig = {
  'pubsweet-server': {
    dbPath,
    adapter: 'leveldb'
  },
  dbManager: {
    username: 'testUsername',
    email: 'test@example.com',
    password: 'test_password',
    collection: 'test_collection'
  }
}

describe('setup-db', () => {
  beforeAll(async () => {
    fs.ensureDirSync(basePath)
    await new PouchDB(dbPath).destroy()
  })

  beforeEach(async () => {
    jest.resetModules() // necessary because db must be recreated after destroyed
    const config = require('config')
    config['pubsweet-server'] = baseConfig['pubsweet-server']
    config['dbManager'] = baseConfig['dbManager']
    const { setupDb } = require('../../src/')
    await setupDb()
  })

  afterEach(async () => {
    // call to models adds global db: see server/src/models/schema :(
    await db.destroy()
  })

  it('creates the database', () => {
    const dbDir = fs.readdirSync(basePath)
    expect(dbDir).toContain('test_db')
    const items = fs.readdirSync(dbPath)
    expect(items).toContain('CURRENT')
  })
})
